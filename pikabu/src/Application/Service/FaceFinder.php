<?php
declare(strict_types=1);

require_once __DIR__ . '/FaceFinderInterface.php';


class FaceFinder implements FaceFinderInterface
{

    /**
     * @var PDO
     */
    private $dbConnection;
    /**
     * @var int
     */
    private $accuracy;
    /**
     * @var bool|PDOStatement
     */
    private $searchStatement;
    /**
     * @var bool|PDOStatement
     */
    private $insertStatement;

    /**
     * FaceFinder constructor.
     * @param array $config
     * @param int $accuracy
     */
    public function __construct(array $config, int $accuracy = 100)
    {
        if ($accuracy > 100 || $accuracy < 0) {
            $accuracy = 0;
        }
        $this->accuracy = $accuracy;

        $this->dbConnection = new PDO('mysql:host=' . $config['host'], $config['user'], $config['password']);

        if (!$this->dbConnection->query('USE face_finder')) {
            $this->dbConnection->exec('CREATE DATABASE face_finder');
            $this->dbConnection->exec('USE face_finder');
        }

        if (!$this->dbConnection->query('DESCRIBE face')) {
            $this->dbConnection->exec('CREATE TABLE `face` (
                `id`      INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `race`    SMALLINT     NOT NULL,
                `emotion` SMALLINT     NOT NULL,
                `oldness` SMALLINT     NOT NULL,
                UNIQUE (`id`)
            ) ENGINE = InnoDB');
        }
        $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this->prepareStatements();
    }

    public function resolve(FaceInterface $face): array
    {
        if ($face->getId() === 0) {
            $this->save($face);
        }

        $searchStatement = $this->search($face);

        $result = [];
        while ($row = $searchStatement->fetch(PDO::FETCH_ASSOC)) {
            $result[] = $this->hydrate($row);
        }
        return $result;
    }

    public function flush(): void
    {
        $this->dbConnection->exec('TRUNCATE `face`');
        $this->dbConnection->exec('ALTER TABLE `face` AUTO_INCREMENT=1');
    }

    private function prepareStatements(): void
    {
        $SQL = 'INSERT INTO `face` (`race`, `emotion`, `oldness`) 
                VALUES (:race, :emotion, :oldness)';

        $this->insertStatement = $this->dbConnection->prepare($SQL);

        $SQL = 'WITH last_faces AS (
                SELECT id, race, emotion, oldness
                FROM face
                ORDER BY id DESC
                LIMIT 10000
            )
            SELECT id, race, emotion, oldness,
                   SQRT(
                       POW(race - :race, 2) + 
                       POW(emotion - :emotion, 2) +
                       POW(oldness - :oldness, 2)
                   ) AS distance
            FROM last_faces AS face
            WHERE (face.race > :race - :raceAccuracy OR face.race < :race + :raceAccuracy)
                AND (face.emotion > :emotion - :emotionAccuracy OR face.emotion < :emotion + :emotionAccuracy)
                AND (face.oldness > :oldness - :oldnessAccuracy OR face.oldness < :oldness + :oldnessAccuracy)
            ORDER BY distance
            LIMIT 5';

        $this->searchStatement = $this->dbConnection->prepare($SQL);
    }

    private function save(FaceInterface $face): void
    {
        $this->insertStatement->bindValue(':race', $face->getRace());
        $this->insertStatement->bindValue(':emotion', $face->getEmotion());
        $this->insertStatement->bindValue(':oldness', $face->getOldness());
        $this->insertStatement->execute();
    }

    private function search(FaceInterface $face)
    {
        $this->searchStatement->bindValue(':race', $face->getRace());
        $this->searchStatement->bindValue(':emotion', $face->getEmotion());
        $this->searchStatement->bindValue(':oldness', $face->getOldness());
        $this->searchStatement->bindValue(':raceAccuracy', $this->accuracy);
        $this->searchStatement->bindValue(':emotionAccuracy', $this->accuracy * 10);
        $this->searchStatement->bindValue(':oldnessAccuracy', $this->accuracy * 10);
        $this->searchStatement->execute();

        return $this->searchStatement;
    }

    private function hydrate(array $data): \Face
    {
        return new Face((int)$data['race'], (int)$data['emotion'], (int)$data['oldness'], (int)$data['id']);
    }
}