<?php
declare(strict_types=1);

require_once __DIR__ . '/FaceInterface.php';

class Face implements FaceInterface
{

    /**
     * @var int
     */
    private $race;
    /**
     * @var int
     */
    private $emotion;
    /**
     * @var int
     */
    private $oldness;
    /**
     * @var int
     */
    private $id;

    public function __construct(int $race, int $emotion, int $oldness, int $id = 0)
    {
        $this->race = $race;
        $this->emotion = $emotion;
        $this->oldness = $oldness;
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getRace(): int
    {
        return $this->race;
    }

    public function getEmotion(): int
    {
        return $this->emotion;
    }

    public function getOldness(): int
    {
        return $this->oldness;
    }
}