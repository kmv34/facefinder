up: docker-up

docker-up:
	docker-compose up --build -d

stop:
	docker-compose stop

run:
	docker-compose exec pikabu-php-cli php index.php